﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleRanking;
using SimpleRankingProtocol;

public class ResultManager : MonoBehaviour {

    [SerializeField] Text resultText;
    [SerializeField] InputField nameInput;

    void Start() {
        resultText.text = GameManager.Instance.Score.ToString();
    }

    public void SendScore() {
        RequestUpdateRanking param = new RequestUpdateRanking();
        param.tableName = "DontCoupleExplotionScore";
        param.playerName = nameInput.text;
        param.score = GameManager.Instance.Score;
        ApiClient.Instance.ResponseUpdateRanking = (ResponseUpdateRanking response) => { BackTitle(); };
        ApiClient.Instance.RequestUpdateRanking(param);
    }

    public void BackTitle() {
        GameManager.Instance.SceneChange("Title", 3f, Color.black);
    }

}
