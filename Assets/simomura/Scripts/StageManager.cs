﻿using UnityEngine;
using System.Collections;

public class StageManager : MonoBehaviour {

    public static StageManager Instance;
    public int SpawnCount = 0;

    enum SpawnMode {
        Nomal, CrepeWagonMode, SantaMode
    }

    [Header("異性カップルは要素1番目に")]
    [SerializeField] Couple[] couplePrefabs;
    [SerializeField] Transform coupleSpawnTrans;
    [SerializeField] int maxCoupleCount;
    [SerializeField] float nomalCoupleSpawnRate;
    [SerializeField] float spawnRadius;
    [SerializeField] float minSpawnTimeDistance;
    [SerializeField] float maxSpawnTimeDistance;
    [SerializeField] float santaModeSpawnTimeDistance;
    [SerializeField] float minCoupleLifeTime;
    [SerializeField] float maxCoupleLifeTime;
    [Space]
    [SerializeField] float santaModeTimeDistance;
    [SerializeField] float santaModeTime;
    [Space]
    [SerializeField] CrepeWagon wagonPrefab;
    [SerializeField] float wagonModeTimeDistance;
    [SerializeField] float wagonStayTime;
    
    SpawnMode spawnMode = SpawnMode.Nomal;
    int score;

    public int Score {
        get {
            return score;
        }
        set {
            score = value;
        }
    }

    public float MinCoupleLifeTime {
        get { return minCoupleLifeTime; }
    }

    public float MaxCoupleLifeTime {
        get { return maxCoupleLifeTime; }
    }

    void Awake() {
        Instance = this;
        SpawnCount = 0;
    }

    void Start() {
        StartCoroutine(SpawnCoroutine());
        StartCoroutine(SantaModeCoroutine());
        StartCoroutine(CrepeWagonModeCoroutine());
    }

    IEnumerator SpawnCoroutine() {
        while (true) {
            Couple couple = null;
            Vector3 pos = coupleSpawnTrans.position;
            switch (spawnMode) {
                case SpawnMode.Nomal:
                    pos = new Vector3(Random.Range(-spawnRadius, spawnRadius), Random.Range(-spawnRadius, spawnRadius));
                    couple = GetCouple();
                    break;
                case SpawnMode.CrepeWagonMode:
                    pos = new Vector3(Random.Range(-spawnRadius, spawnRadius), Random.Range(-spawnRadius, spawnRadius));
                    couple = GetCouple();
                    break;
                case SpawnMode.SantaMode:
                    pos = new Vector3(Random.Range(-spawnRadius, spawnRadius), Random.Range(-spawnRadius, spawnRadius));
                    couple = GetCouple();
                    break;
            }
            if (SpawnCount <= maxCoupleCount) {
                Couple c = Instantiate(GetCouple(), coupleSpawnTrans.position + pos, coupleSpawnTrans.rotation) as Couple;
                c.Init(Random.Range(minCoupleLifeTime, maxCoupleLifeTime));
                Debug.Log("Couple Spawn");
            }

            if(spawnMode == SpawnMode.SantaMode) {
                yield return new WaitForSeconds(santaModeSpawnTimeDistance);
                continue;
            }

            yield return new WaitForSeconds(Random.Range(minSpawnTimeDistance, maxSpawnTimeDistance));
        }
    }

    public GameObject santaPrefab;
    public AudioSource AudioSourceBGM;
    public AudioSource AudioSourceSanta;
    IEnumerator SantaModeCoroutine() {
        while (true) {
            yield return new WaitForSeconds(santaModeTimeDistance);
            //サンタモード
            Debug.Log("サンタモード開始");
            GameObject santa = null;
            if (spawnMode != SpawnMode.CrepeWagonMode) { 
                StopCoroutine(SpawnCoroutine());
                spawnMode = SpawnMode.SantaMode;
                StartCoroutine(SpawnCoroutine());
                santa = Instantiate(santaPrefab);
                AudioSourceBGM.gameObject.SetActive(false);
                AudioSourceSanta.gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(santaModeTime);
            spawnMode = SpawnMode.Nomal;
            Destroy(santa);
            AudioSourceBGM.gameObject.SetActive(true);
            AudioSourceSanta.gameObject.SetActive(false);
            Debug.Log("サンタモード終了");
        }
    }

    IEnumerator CrepeWagonModeCoroutine() {
        while(true){
            yield return new WaitForSeconds(wagonModeTimeDistance);
            //ワゴン開始
            Debug.Log("クレープワゴン開始");
            spawnMode = SpawnMode.CrepeWagonMode;
            CrepeWagon c = Instantiate(wagonPrefab) as CrepeWagon;
            yield return new WaitForSeconds(wagonStayTime);
            //ワゴン終了
            spawnMode = SpawnMode.Nomal;
            c.crepeWagonEnd();
            Debug.Log("クレープワゴン終了");
        }
    }

    public void SpawnCoupleImmediately() {
        Vector3 pos = new Vector3(Random.Range(-spawnRadius, spawnRadius), Random.Range(-spawnRadius, spawnRadius));
        Couple c = Instantiate(GetCouple(), coupleSpawnTrans.position + pos, coupleSpawnTrans.rotation) as Couple;
        c.Init(Random.Range(minCoupleLifeTime, maxCoupleLifeTime));
        Debug.Log("Couple Spawn");
    }

    public Couple GetCouple() {
        float r = Random.Range(0, 1f);
        Couple couple = null;
        if(r <= nomalCoupleSpawnRate) {
            couple = couplePrefabs[0];
        }else {
            couple = couplePrefabs[Random.Range(1, 2)];
        }
        return couple;
    }

    public void GameOver() {
        Debug.Log("GameOver");
        GameManager.Instance.Score = score;
        GameManager.Instance.SceneChange("Result", 3f, Color.black);
    }

}
