﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CoupleUI : MonoBehaviour {

    [SerializeField] RectTransform uiPrefab;

    GameObject uiGameObject;
    Text uiText;
    Couple couple;
    
    public bool IsDisplay {
        get { return uiText != null; }
    }

    void Start() {
        couple = GetComponent<Couple>();
    }

    public void Init() {
        if (couple.IsConnectCouple) {
            Transform canvasTrans = GameObject.FindGameObjectWithTag("GameController").transform;
            Transform uitrans = Instantiate(uiPrefab.transform) as Transform;
            uitrans.SetParent(canvasTrans, false);
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos.z = 0;
            uitrans.position = pos;

            uiGameObject = uitrans.gameObject;

            uiText = uiGameObject.GetComponent<Text>();
            uiText.enabled = true;
        }
    }

    public void SetTime(float time) {
        uiText.text = ((int)time).ToString();
    }

    public void DestroyUI() {
        Debug.Log("UIDestory");
        Debug.Log(uiGameObject);
        Destroy(uiGameObject);
    }


}
