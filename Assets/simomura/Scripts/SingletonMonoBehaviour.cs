﻿using UnityEngine;
using System.Collections;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T: MonoBehaviour {

	private static T instance;

	public static T Instance {
		get {
			if (instance == null) {
				instance = (T)FindObjectOfType (typeof(T));

				if (instance == null) {
                    Debug.LogError (typeof(T) + " is nothing");
				}
			}

			return instance;
		}
	}

	[SerializeField] bool m_DontDestoryOnLoad = true;

	public virtual void Awake (){
		if (this != Instance) {
			Destroy (this.gameObject);
			return;
		}

		if (m_DontDestoryOnLoad) {
			DontDestroyOnLoad (this.gameObject);
		}
	}

}