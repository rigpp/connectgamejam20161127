﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleRanking;
using SimpleRankingProtocol;

public class RankingManager : MonoBehaviour {

    [SerializeField] Text rankingText;

    void Start() {
        RequestShowRanking param = new RequestShowRanking();
        param.tableName = "DontCoupleExplotionScore";
        ApiClient.Instance.ResponseShowRanking = ResponseRanking;
        ApiClient.Instance.RequestShowRanking(param);
        
    }

    void ResponseRanking(ResponseShowRanking response) {
        rankingText.text = string.Empty;
        for (int i = 0; i < response.rankingList.Count; i++) {
            rankingText.text += System.String.Format(
                "{0} PlayerName:{1} 離したカップルの数:{2}\n",
                response.rankingList[i].rank,
                response.rankingList[i].playerName,
                response.rankingList[i].score);
        }
    }

    public void BackTitle() {
        GameManager.Instance.SceneChange("Title", 3f, Color.black);
    }
}
