﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : SingletonMonoBehaviour<GameManager> {

	[HideInInspector] public bool IsStagePlaying = false;
	[HideInInspector] public bool IsSceneChangeing = false;
    [HideInInspector] public int Score;
	public readonly string TitleSceneName = "StageSelect";

	[SerializeField] Image m_FadeImage;
	bool IsFadeOut = false;
	float m_BGMVolume = 0.2f;
	float m_SEVolume = 0.9f;

	public override void Awake (){
		base.Awake ();
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	public float BGMVolume{
		set{ 
			m_BGMVolume = Mathf.Clamp01 (value);
		}
		get{ return m_BGMVolume; }
	}

	public float SEVolume{
		set{ m_SEVolume = Mathf.Clamp01 (value);}
		get{ return m_SEVolume;}
	}


	void OnSceneLoaded(Scene scene,LoadSceneMode mode){

	}

	public void SceneChange(string sceneName, float time, Color color){
		if(!IsSceneChangeing){
			StartCoroutine (SceneChangeCoroutine (sceneName,time,color));
		}
	}
		

	IEnumerator SceneChangeCoroutine(string sceneName,float time,Color color){
		IsSceneChangeing = true;
		m_FadeImage.gameObject.SetActive (true);
		color.a = 0;
		m_FadeImage.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_FadeImage.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;
			m_FadeImage.color= c;
			t += Time.deltaTime;
			yield return null;
		}

		t = 0;
		c = m_FadeImage.color;
		SceneManager.LoadScene (sceneName);
		yield return null;

		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_FadeImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_FadeImage.gameObject.SetActive (false);
		IsSceneChangeing = false;
	}

	public void FadeOut(float time,float stopTime,Color color){
		if (!IsFadeOut) {
			StartCoroutine (FadeOutCoroutine (time,stopTime,color));
		}
	}

	IEnumerator FadeOutCoroutine(float time,float stopTime,Color color){
		IsFadeOut = true;
		m_FadeImage.gameObject.SetActive (true);
		color.a = 0;
		m_FadeImage.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_FadeImage.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;;
			m_FadeImage.color= c;
			t += Time.deltaTime;
			yield return null;
		}
		t = 0;
		c = m_FadeImage.color;

		if (stopTime <= 0)  {
			yield return null;
		} else {
			yield return new WaitForSeconds(stopTime);
		}
		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_FadeImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_FadeImage.gameObject.SetActive (false);
		IsFadeOut = false;
	}

}
