﻿using UnityEngine;
using System.Collections;

public class Couple : MonoBehaviour {

    readonly string playerTag = "Player";

    [SerializeField] float coupleFadeOutTime;
    [SerializeField] float coupleDistance;
    [Header("要素0番目が左、1番目が右の人")]
    [SerializeField] GameObject[] coupleObject;
    [SerializeField] Sprite[] couple1Spirtes;
    [SerializeField] Sprite[] couple2Sprites;
    [Space]
    [SerializeField] float checkOtherCoupleRadius;

    Rigidbody2D rigidbody2d;
    SpriteRenderer[] coupleSprite;
    CoupleUI coupleUI;
    bool isConnectCouple = true;
    bool isTimeCount = true;
    float correntTime = 0;
    int coupleid;

    public bool IsConnectCouple {
        get { return isConnectCouple; }
    }

    public int CoupleID {
        get { return coupleid; }
    }

    public void Init(float time) {
        correntTime = time;
    }

    void Awake() {
        rigidbody2d = GetComponent<Rigidbody2D>();

        Vector3 couplePos = new Vector3(coupleDistance / 2f, 0, 0);
        coupleObject[0].transform.position = transform.position - couplePos;
        coupleObject[1].transform.position = transform.position + couplePos;

        Vector2 viewPortPos = Camera.main.WorldToViewportPoint(transform.position);
        if (viewPortPos.x > 0.9f ||
            viewPortPos.y > 0.9f ||
            viewPortPos.x < 0.1f ||
            viewPortPos.y < 0.1f) {
            Debug.Log("画面外");
            //StageManager.Instance.SpawnCoupleImmediately();
            Destroy(gameObject);
        }

        coupleSprite = GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < coupleSprite.Length; i++) {
            coupleSprite[i].enabled = false;
        }

        coupleUI = GetComponent<CoupleUI>();
        //RaycastHit2D hit = Physics2D.CircleCast(transform.position, checkOtherCoupleRadius, Vector3.zero);
        //if (hit.transform != null) {
        //    if (hit.transform.root.tag == this.tag) {
        //        if (hit.transform.root.GetInstanceID() == GetInstanceID()) {
        //            Debug.Log("Destroy");
        //            Destroy(gameObject);
        //        }
        //    }
        //}
    }

    IEnumerator Start() {
        coupleid = StageManager.Instance.SpawnCount++;
        yield return null;
        for(int i = 0; i < coupleSprite.Length; i++) {
            coupleSprite[i].enabled = true;
        }
        coupleSprite[0].sprite = couple1Spirtes[Random.RandomRange(0, couple1Spirtes.Length)];
        coupleSprite[1].sprite = couple2Sprites[Random.RandomRange(0, couple2Sprites.Length)];
        coupleUI.Init();
    }

    void Update() {
        if (isConnectCouple && isTimeCount) {
            correntTime -= Time.deltaTime;
            if(correntTime <= 0) {
                isTimeCount = false;
                correntTime = 0;
                StageManager.Instance.GameOver();
            }
        }
        if (coupleUI.IsDisplay) {
            coupleUI.SetTime(correntTime);
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.transform.root.tag == playerTag) {
            SeparateCouple();
        }

        if (other.transform.root.tag == this.tag) {
            if (other.transform.parent.GetComponent<Couple>().CoupleID <= coupleid) {
                Debug.Log("被り");
                coupleUI.DestroyUI();
                //StageManager.Instance.SpawnCoupleImmediately();
                Destroy(gameObject);
            }
        }
    }

    void SeparateCouple() {
        if (isConnectCouple) {
            Debug.Log("Separete");
            isConnectCouple = false;
            coupleUI.DestroyUI();

            Destroy(rigidbody2d);
            this.tag = "Untagged";
            Rigidbody2D[] coupleRigids = new Rigidbody2D[coupleObject.Length];
            for(int i = 0; i < coupleRigids.Length; i++) {
                coupleRigids[i] = coupleObject[i].AddComponent<Rigidbody2D>();
                coupleRigids[i].drag = 0;
                coupleRigids[i].angularDrag = 0;
                coupleRigids[i].gravityScale = 0;
            }
            float separatePower = 1f;
            float separateTorque = 10f;
            coupleRigids[0].AddForce(new Vector2(-separatePower, 0),ForceMode2D.Impulse);
            coupleRigids[0].AddTorque(separateTorque);
            coupleRigids[1].AddForce(new Vector2(separatePower, 0), ForceMode2D.Impulse);
            coupleRigids[1].AddTorque(separateTorque * -1f);

            StartCoroutine(FadeOutCoroutine());

            StageManager.Instance.SpawnCount--;
            StageManager.Instance.Score++;
        }
    }

    IEnumerator FadeOutCoroutine() {
        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
        Color c = sprites[0].color;
        float deltaAlpha = 1f / coupleFadeOutTime;
        float t = 0;
        while (t <= coupleFadeOutTime) {
            for (int i = 0; i < sprites.Length; i++) {
                c.a -= deltaAlpha * Time.deltaTime;
                sprites[i].color = c;
                t += Time.deltaTime;
            }
            yield return null;
        }

        Destroy(gameObject);
    }

}
