﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleManager : MonoBehaviour {

    [SerializeField] ScrollRect titleScroll;
    [SerializeField] RectTransform buttons;
    [SerializeField] float speed;

    public void GameStart() {
        buttons.gameObject.SetActive(false);
        StartCoroutine(GameStartCoroutine());
    }

    IEnumerator GameStartCoroutine() {
        float p = 0;
        Debug.Log(titleScroll.verticalNormalizedPosition);
        while (titleScroll.verticalNormalizedPosition >= 0f) {
            titleScroll.verticalNormalizedPosition -= speed * Time.deltaTime;
            yield return null;
        }
        GameManager.Instance.SceneChange("Main", 3f, Color.black);
    }


    public void SceneChangeRanking() {
        GameManager.Instance.SceneChange("Ranking", 3f, Color.black);
    }

    public void Quit() {
        Application.Quit(); 
    }
}
