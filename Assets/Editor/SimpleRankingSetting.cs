﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using SimpleRankingProtocol;

namespace SimpleRanking{
	[InitializeOnLoad]
	public class SimpleRankingSetting {

		static SimpleRankingSetting(){

			ProjectInfo info = new ProjectInfo ();
			info.productName = PlayerSettings.productName;
			info.productGUID = PlayerSettings.productGUID.ToString ();
			System.IO.File.WriteAllText ("Assets/Resources/SimpleRanking.json", JsonUtility.ToJson (info, true));
			//AssetDatabase.Refresh ();
		}

	}
}
