﻿using UnityEngine;
using System.Collections;
//ランキング送受信に必要な名前空間2つ
using SimpleRanking;
using SimpleRankingProtocol;

public class Sample : MonoBehaviour {

	[SerializeField] UnityEngine.UI.Text rankingText;

	void Update(){
		if (Input.GetKeyDown (KeyCode.Return)) {
			//ランキング取得用のパラメータを作成
			RequestShowRanking param = new RequestShowRanking ();
			//ランキングを取得するテーブル名,テーブルがない場合は自動生成される
			//DBは部内共通なので他ゲームとのテーブル名被りに注意すること
			//なお、"TestTable"のみはデバッグ用にアクセス権を設定していない
			param.tableName = "TestTable";
			//ランキング取得時のコールバックを登録
			ApiClient.Instance.ResponseShowRanking = ResponseRanking;
			//ランキング取得のリクエストを送信
			ApiClient.Instance.RequestShowRanking (param);
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			//スコア送信用のパラメータを作成
			RequestUpdateRanking param = new RequestUpdateRanking ();
			param.tableName = "TestTable";
			param.playerName = "SimpleRanker";
			param.score = 500;
			//スコア送信時のコールバックを登録
			ApiClient.Instance.ResponseUpdateRanking = ResponseSendScore;
			//スコア送信
			ApiClient.Instance.RequestUpdateRanking (param);
		}
	}

	//ランキング取得時のコールバック
	//responseにランキング情報が入っている
	void ResponseRanking(ResponseShowRanking response){
		rankingText.text = string.Empty;
		for (int i = 0; i < response.rankingList.Count; i++) {
			Debug.Log (System.String.Format (
				"{0} PlayerName:{1} Score:{2}",
				response.rankingList [i].rank,
				response.rankingList [i].playerName,
				response.rankingList [i].score)
			);
			rankingText.text += System.String.Format (
				"{0} PlayerName:{1} Score:{2}\n",
				response.rankingList [i].rank,
				response.rankingList [i].playerName,
				response.rankingList [i].score);
		}
	}

	//ランキング取得時のコールバック
	//responseに送信したスコア情報が入っている
	void ResponseSendScore(ResponseUpdateRanking response){
		Debug.Log ("スコア送信完了");
	}

}
