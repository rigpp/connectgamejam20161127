﻿#pragma warning disable 0414
using UnityEngine;
using System;
using System.Collections.Generic;

namespace SimpleRankingProtocol{
	
	[Serializable]
	public class RequestShowRanking{
		[SerializeField]
		string productName;
		[SerializeField]
		string productGUID;
		[SerializeField]
		string mode = "Show";

		public string tableName;

		public RequestShowRanking(){
			try{
				ProjectInfo info = JsonUtility.FromJson<ProjectInfo> (Resources.Load<TextAsset> ("SimpleRanking").text);
				productName = info.productName;
				productGUID = info.productGUID;
			}catch(System.Exception e){
				Debug.LogError ("設定ファイル Resources/SimpleRanking.json の読み込みに失敗しました\n" + e.StackTrace);
			}
		}
	}

	[Serializable]
	public class ResponseShowRanking{
		public List<RankInfo> rankingList;
	}

	[Serializable]
	public class RankInfo{
		public int rank;
		public string playerName;
		public int score;
	}

	[Serializable]
	public class RequestUpdateRanking{
		[SerializeField]
		string productName;
		[SerializeField]
		string productGUID;
		[SerializeField]
		string mode = "Update";

		public string tableName;
		public string playerName;
		public int score;

		public RequestUpdateRanking(){
			try{
				ProjectInfo info = JsonUtility.FromJson<ProjectInfo> (Resources.Load<TextAsset> ("SimpleRanking").text);
				productName = info.productName;
				productGUID = info.productGUID;
			}catch(System.Exception e){
				Debug.LogError ("設定ファイル Resources/SimpleRanking.json の読み込みに失敗しました\n" + e.StackTrace);
			}
		}
	}

	[Serializable]
	public class ResponseUpdateRanking{
		public int rank;
		public string playerName;
		public int score;
	}

	[Serializable]
	public class ProjectInfo{
		public string productName;
		public string productGUID;
	}

}