﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace SimpleRanking{
	public class Requester {

		const float timeOut = 5.0f;
		public MonoBehaviour monoBehaivior;

		public Requester(MonoBehaviour monoBehaivior){
			this.monoBehaivior = monoBehaivior;
		}

		public void Request<SEND,RESPONSE>(string url, SEND sendData,UnityAction<RESPONSE> callback){
			string data = JsonUtility.ToJson (sendData);
			Debug.Log (monoBehaivior);
			monoBehaivior.StartCoroutine (RequestCoroutine (url, data, callback));
		}

		IEnumerator RequestCoroutine<RESPONSE>(string url, string data, UnityAction<RESPONSE> callback){
			WWWForm wwwForm = new WWWForm ();
			wwwForm.AddField ("Data", data);
			WWW www = new WWW (url, wwwForm);

			yield return monoBehaivior.StartCoroutine (CheckTimeOut (www, Requester.timeOut));

			if (string.IsNullOrEmpty (www.text)) {
				Debug.LogError (www.error);
				yield break;
			}

			try{
				RESPONSE response = JsonUtility.FromJson<RESPONSE>(www.text);
				callback(response);
			}catch(System.ArgumentException e){
				Debug.LogErrorFormat ("Request Error\n{0}\n{1}", www.text, e.StackTrace);
			}
		}

		IEnumerator CheckTimeOut(WWW www, float timeout){
			float requestTime = Time.time;

			while(!www.isDone){
				if (Time.time - requestTime < timeout) {
					yield return null;
				}else{
					Debug.LogError("TimeOut"); 
					break;
				}
			}
			yield return null;
		}

	}
}
