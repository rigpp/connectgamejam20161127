﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleRankingProtocol;

namespace SimpleRanking{
	public class ApiClient : SingletonMonoBehaviour<ApiClient> {

		string url = "http://rigpp.sakura.ne.jp/simomutter/SimpleRanking/SimpleRanking.php";
		Requester requester;

		public override void Awake (){
			base.Awake ();
			requester = new Requester (this);
		}

		/// <summary>
		/// RequestShowRanking
		/// サーバーにランキングをリクエストする
		/// </summary>
		public void RequestShowRanking(RequestShowRanking param){
			requester.Request<RequestShowRanking,ResponseShowRanking> (url, param, ResponseShowRanking);
		}
			
		/// <summary>
		/// ResponseShowRanking
		/// RequestShowRankingを行った時のコールバックを登録する
		/// </summary>
		public UnityAction<ResponseShowRanking> ResponseShowRanking;

		/// <summary>
		/// RequestUpdateRanking
		/// サーバーに得点を送信する
		/// </summary>
		public void RequestUpdateRanking(RequestUpdateRanking param){
			requester.Request<RequestUpdateRanking,ResponseUpdateRanking> (url, param, ResponseUpdateRanking);
		}

		/// <summary>
		/// ResponseUpdateRanking
		/// RequestUpdateRankingを行った時のコールバックを登録する
		/// </summary>
		public UnityAction<ResponseUpdateRanking> ResponseUpdateRanking;

	}
}