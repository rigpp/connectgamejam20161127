﻿using UnityEngine;
using System.Collections;

namespace SimpleRanking{
	public class SingletonMonoBehaviour<T> : MonoBehaviour where T: MonoBehaviour {

		private static T instance;

		public static T Instance {
			get {
				if (instance == null) {
					instance = (T)FindObjectOfType (typeof(T));

					if (instance == null) {
						GameObject go = new GameObject (typeof(T).ToString ());
						go.AddComponent<T> ();
					}
				}

				return instance;
			}
		}

		public virtual void Awake (){
			if (this != Instance) {
				Destroy (this.gameObject);
				return;
			}

			DontDestroyOnLoad (this.gameObject);
		}

	}
}