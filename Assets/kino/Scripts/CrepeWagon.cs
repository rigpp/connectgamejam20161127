﻿using UnityEngine;
using System.Collections;

public class CrepeWagon : MonoBehaviour {

    public float coupleLength;
    private AudioSource[] audioSources;
    private bool isEnd = false;

    // Use this for initialization
    void Start () {
        audioSources = GetComponents<AudioSource>();
        Invoke("spawn", 5.0f);
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    private void spawn() {
        if (isEnd == false) {
            //coupleもらってくる
            Couple cPrefab = StageManager.Instance.GetCouple();

            int coupleNumber = this.transform.childCount;
            Vector3 spawnPosition = transform.position;
            spawnPosition.y -= (coupleNumber + 1) * coupleLength;

            if (coupleNumber >= 3) {
                Invoke("spawn", 4.0f);
            }

            Couple c = Instantiate(cPrefab, spawnPosition, Quaternion.identity) as Couple;
            c.Init(10);
            c.transform.parent = this.transform;
            Invoke("spawn", 4.0f);
        }
    }

    public void crepeWagonEnd() {
        isEnd = true;
        transform.DetachChildren();
        audioSources[1].Play();
        Invoke("destroyCrepeWagon", audioSources[1].clip.length);
    }

    private void destroyCrepeWagon() {
        Destroy(this.gameObject);
    }
}
